package Rooms;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Promos")

public class Promo implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int PromoId;

    @ColumnInfo(name = "Notification Title")
    public String NotificationTitle;

    @ColumnInfo(name = "Notification Message")
    public String NotificationMessage;

    @ColumnInfo(name = "Notification Image")
    public String NotificationImage;

    public int getPromoId() {
        return PromoId;
    }

    public void setPromoId(int promoId) {
        PromoId = promoId;
    }

    public String getNotificationTitle() {
        return NotificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        NotificationTitle = notificationTitle;
    }

    public String getNotificationMessage() {
        return NotificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        NotificationMessage = notificationMessage;
    }

    public String getNotificationImage() {
        return NotificationImage;
    }

    public void setNotificationImage(String notificationImage) {
        NotificationImage = notificationImage;
    }
}
