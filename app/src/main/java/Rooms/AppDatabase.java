package Rooms;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Promo.class}, version = 1,exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {


    public abstract PromoDAO promoDAO();


}