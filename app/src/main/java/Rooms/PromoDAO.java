package Rooms;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PromoDAO {
    @Query("SELECT * FROM promos")
    List<Promo> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertPromo(Promo promo);


}
