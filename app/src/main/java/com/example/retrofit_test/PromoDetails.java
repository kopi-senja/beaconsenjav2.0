package com.example.retrofit_test;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class PromoDetails extends AppCompatActivity {

    TextView title;
    TextView isi;
    ImageButton backbutton;
    ImageView img_view;
    ProgressDialog  progressDialog;
    ProgressBar loadingimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_promo__details);
        title = findViewById(R.id.promo_title);
        isi = findViewById(R.id.isi_promo);
        img_view = findViewById(R.id.promo_image);
        loadingimage = findViewById(R.id.progressbar);

        Typeface customfont_isi= Typeface.createFromAsset(getAssets(),"font/Roboto-Regular.ttf");
        isi.setTypeface(customfont_isi);
        Typeface customfont_title= Typeface.createFromAsset(getAssets(),"font/CreteRound-Regular.ttf");
        title.setTypeface(customfont_title);

        Intent intent = getIntent();
        String titles = intent.getStringExtra("Titles");
        String isis = intent.getStringExtra("Isi_Promo");
        String getImageurl = intent.getStringExtra("Img_Promo");

            Glide.with(PromoDetails.this)
                    .load(getImageurl).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    loadingimage.setVisibility(View.GONE);

                    return true;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    loadingimage.setVisibility(View.GONE);

                    return false;
                }
            })
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(Glide.with(img_view).load(getImageurl))
                    .into(img_view);

        title.setText(titles);
        isi.setText(isis);
        img_view.setImageDrawable(Drawable.createFromPath(getImageurl));

        backbutton=findViewById(R.id.btn_back);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(PromoDetails.this,MainActivity.class);
                finish();
            }
        });


    }
}
