package com.example.retrofit_test;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.support.annotation.RequiresApi;

public class ShowNotification extends ContextWrapper {
    public static final String CHANNEL_1_ID = "channel1";//tarik id toko
    public static final String CHANNEL_NAME = "Notify App";

    private NotificationManager notificationManager;
    public ShowNotification(Context base) {
        super(base);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            createChannel();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannel(){
        //API26++
        NotificationChannel channel = new NotificationChannel(CHANNEL_1_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription("Test");
        channel.enableLights(false);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getNotificationManager().createNotificationChannel(channel);

    }
    public NotificationManager getNotificationManager() {
        if(notificationManager == null){
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    //API26 lower
    public Notification.Builder getNotification(String title, String message, PendingIntent pendingIntent,
                                                boolean isClicked){

        return new Notification.Builder(getApplicationContext(),CHANNEL_1_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.icon_mypps02_1)
                .setContentIntent(pendingIntent)
                .setAutoCancel(isClicked);

    }
}




