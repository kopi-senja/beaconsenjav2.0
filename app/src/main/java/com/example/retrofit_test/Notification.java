package com.example.retrofit_test;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Notification {

    @SerializedName("title")
    @Expose
    private String notificationTitle;
    @SerializedName("message")
    @Expose
    private String notificationMessage;
    @SerializedName("fileName")
    @Expose
    private String notificationImage;


    public Notification(String notificationTitle, String notificationMessage, String notificationImage) {
        this.notificationTitle = notificationTitle;
        this.notificationMessage = notificationMessage;
        this.notificationImage = notificationImage;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }



    public String getNotificationImage() {
        return notificationImage;
    }
}
