package com.example.retrofit_test;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface RequestInterface {



    @POST("/v1/store")
    Call<MajorBeacon> postMajorID(@Body MajorBeacon MajorID);

    @GET("/v1/offerStore/getOffer")
    Call<NotificationList> setDataNotif();

    @POST("/v1/offer/showOfferAndroid")
    Call<NotificationList> postDataNotif(@Body NotificationList MajMin);

    @DELETE("/v1/offerStore/")
    Call<NotificationList> deleteDataNotif();

    @GET("/v1/store")
    Call<NotificationList> setresponseNotif();

}