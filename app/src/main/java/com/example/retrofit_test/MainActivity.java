package com.example.retrofit_test;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import Rooms.AppDatabase;
import Rooms.DatabaseClient;
import Rooms.Promo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static com.example.retrofit_test.ShowNotification.CHANNEL_1_ID;


public class MainActivity extends AppCompatActivity implements BeaconConsumer {

    private RecyclerView recyclerView;
    private TextView alert;
    private ArrayList<Notification> NotifList;
    private static MainActivity mInstance;
    private AppDatabase db;
    private ArrayList abc;
    private DataAdapter adapter;
    protected static final String TAG = "RangingActivity";
    protected static final String TAG2 = "RangingActivity";
    protected static final String TAG3 = "RangingActivity";
    protected static final String TAG4 = "RangingActivity";
//    Databases.AppDatabase db;
    private BeaconManager beaconManager;
    int flag=0;
    ArrayList<String> beaconIds = new ArrayList<>();
    Button delete;
    ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestLocationPermission();
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "AppDatabase").build();
        initViews();
        loadStorage();
//        db.promoDAO().getAll();
//        initViews();




    }

    @Override
    public void onStart(){
        super.onStart();
        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.bind(this);
        alert = findViewById(R.id.alert_notfound);


    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public static synchronized MainActivity getInstance() {
        return mInstance;
    }
    public AppDatabase getDatabase(){
        return db;
    }

    private void requestLocationPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_NOTIFICATION_POLICY)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }
                        else  {
                            Toast.makeText(getApplicationContext(), "Some permission Denied", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void initViews(){

        recyclerView = findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
//        loadJSON();

    }

    private void loadJSON(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.58.89.5:8900")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<NotificationList> call = request.setDataNotif();

            call.enqueue(new Callback<NotificationList>() {

                @Override
                public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {
                        alert.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                    try{
//
//                        NotificationList jsonResponse = response.body();
//                        NotifList = new ArrayList<>(Arrays.asList(jsonResponse.dataNotif()));
//                        adapter = new DataAdapter(getApplicationContext(),NotifList);
//                        recyclerView.setAdapter(adapter);






                    }
                    catch (Exception e){
                        alertDialog();
                    }


                }

                @Override
                public void onFailure(Call<NotificationList> call, Throwable t) {
                    alert.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            });
        }


    private void insertData(final Promo promo){

        new AsyncTask<Void, Void, Long>(){
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses insert data
                long status = db.promoDAO().insertPromo(promo);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
                Toast.makeText(MainActivity.this, "status row "+status, Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }
    private void Post(final NotificationList MajID) {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://testlocal.free.beeceptor.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface requestInterface = retrofit.create(RequestInterface.class);
        Call<NotificationList> call = requestInterface.postDataNotif(MajID);

        call.enqueue(new Callback<NotificationList>() {
            @Override
            public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {


                Log.i(TAG,"Success");
                initViews();
                NotificationList jsonResponse = response.body();
                NotifList = new ArrayList<>(Arrays.asList(jsonResponse.dataNotif()));
                for (int i = 0; i < NotifList.size(); i++) {
                    Promo A = new Promo();
                    A.setNotificationTitle(NotifList.get(i).getNotificationTitle());
                    A.setNotificationMessage(NotifList.get(i).getNotificationMessage());
                    A.setNotificationImage(NotifList.get(i).getNotificationImage());
                    insertData(A);
                }
                initViews();
                loadStorage();
//                adapter = new DataAdapter(getApplicationContext(), NotifList);
//                recyclerView.setAdapter(adapter);








            }

            @Override
            public void onFailure(Call<NotificationList> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Hello Something Wrong Bro",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void DeletePromo(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.58.89.5:8900")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);
        Call<NotificationList> call = requestInterface.deleteDataNotif();
        call.enqueue(new Callback<NotificationList>() {
            @Override
            public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {
                finish();

            }

            @Override
            public void onFailure(Call<NotificationList> call, Throwable t) {

            }
        });
    }

    private void GetNotificationData(NotificationList MajID){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://testlocal.free.beeceptor.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);
        Call<NotificationList> call = requestInterface.postDataNotif(MajID);
        call.enqueue(new Callback<NotificationList>() {
            @Override
            public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {


                    NotificationList notificationList = response.body();
                    NotifList = new ArrayList<>(Arrays.asList(notificationList.dataNotif()));
                    for (int i = 0; i < NotifList.size(); i++) {
                        String Titles = NotifList.get(i).getNotificationTitle();
                        String Messages = NotifList.get(i).getNotificationMessage();
                        String image = NotifList.get(i).getNotificationImage();
                        sendOnChannell(Titles, Messages, image);

                    }





            }

            @Override
            public void onFailure(Call<NotificationList> call, Throwable t) {
                Toast.makeText(MainActivity.this, "fail", Toast.LENGTH_SHORT).show();

            }
        });

    }



    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {


    }
    public void sendOnChannell (String title, String message , String image) {
        int m = (int) ((new Date().getTime() / 1000L)% Integer.MAX_VALUE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //API 26 and lower
            ShowNotification notification;
            android.app.Notification.Builder builder;
            Intent intent = new Intent(MainActivity.this, PromoDetails.class);
            intent.putExtra("Titles",title);
            intent.putExtra("Isi_Promo",message);
            intent.putExtra("Img_Promo",image);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            notification = new ShowNotification(this);
            builder = notification.getNotification(title, message, pendingIntent, true);
            notification.getNotificationManager().notify(m, builder.build());
        } else {
            //API 26 higher
            Intent intent = new Intent(MainActivity.this, PromoDetails.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("Titles",title);
            intent.putExtra("Isi_Promo",message);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                    .setSmallIcon(R.drawable.icon_mypps02_1)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            NotificationManager notificationManager = (NotificationManager)
                    getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(m, builder.build());
        }
       flag++;

    }


    @Override
    public void onBeaconServiceConnect() {

        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {

            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {


                    if (beacons.size() > 0) {
                            if (beacons.iterator().next().getDistance() < 1) {
                                if (!beaconIds.contains(beacons.iterator().next().getId2().toString())) {
                                    Log.i(TAG, "Banyak Beacon" + beacons.size());
                                    Log.i(TAG,"Banyak"+beacons.iterator().next().getId2() +beacons.iterator().next().getId3());
                                    beaconIds.add(beacons.iterator().next().getId2().toString());
                                    NotificationList MajID = new NotificationList(
                                            beacons.iterator().next().getId2().toString(),
                                            beacons.iterator().next().getId3().toString());


                                    Post(MajID);
                                    GetNotificationData(MajID);

                                }


                        }
                    }
                }
//            }



                });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {    }


    }

    public void alertDialog(){
                AlertDialog.Builder alert = new AlertDialog.Builder
                        (new ContextThemeWrapper(MainActivity.this,
                                R.style.AlertDialogCustom));
                alert.setTitle("We are sorry ");
                alert.setMessage("Our API is dead, Application will be closed").
                setNeutralButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                            MainActivity.this.finish();
                    }
                });
             AlertDialog alertDialog = alert.create();
             alertDialog.show();


    }

    private void loadStorage(){

            class GetPromo extends AsyncTask<Void, Void, List<Promo>> {

                @Override
                protected List<Promo> doInBackground(Void... voids) {
                    List<Promo> promoList = DatabaseClient
                            .getInstance(getApplicationContext())
                            .getAppDatabase()
                            .promoDAO()
                            .getAll();
                    return promoList;
                }

                @Override
                protected void onPostExecute(List<Promo> promo) {
                    super.onPostExecute(promo);
                    if(promo.size()==0){
                        alert = findViewById(R.id.alert_notfound);
                        alert.setVisibility(View.VISIBLE);
                    }else if(promo.size()!=0) {
                        alert.setVisibility(View.GONE);
                        DataAdapter adapter = new DataAdapter(MainActivity.this, promo);
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            GetPromo gt = new GetPromo();
            gt.execute();
        }




}