package com.example.retrofit_test;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationList {

    @SerializedName("major")
    @Expose
    private String majornotification;
    @SerializedName("minor")
    @Expose
    private String minornotification;

    public NotificationList(String majornotification, String minornotification) {
        this.majornotification = majornotification;
        this.minornotification = minornotification;
    }


    private Notification[] offers;

//    public Notification[] getOffers() {
//        return offers;
//    }

    public Notification[] dataNotif() {
        return offers;
    }

}