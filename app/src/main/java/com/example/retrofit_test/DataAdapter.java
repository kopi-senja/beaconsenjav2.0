package com.example.retrofit_test;



import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import Rooms.Promo;


public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    List<Promo> promoList;
    private Context context;
    protected static final String TAG = "RangingActivity";

    public DataAdapter(Context context, List<Promo> promoList) {
        this.context = context;
        this.promoList = promoList;


    }





    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        viewHolder.tv_name.setText(promoList.get(i).NotificationTitle);
        viewHolder.tv_api_level.setText(promoList.get(i).NotificationMessage);
        String image_link = promoList.get(i).NotificationImage;
        viewHolder.image.setImageResource(R.drawable.applogo);
        Glide.with(viewHolder.image.getContext())
                .load(image_link).override(1000,500)
                .into(viewHolder.image);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {


                    Intent intent = new Intent(view.getContext(), PromoDetails.class);
                Log.i(TAG, "ini gambar"+promoList.get(i).NotificationImage);
                    intent.putExtra("Titles", promoList.get(i).getNotificationTitle());
                    intent.putExtra("Isi_Promo", promoList.get(i).getNotificationMessage());
                    intent.putExtra("Img_Promo", promoList.get(i).getNotificationImage());
                view.getContext().startActivity(intent);


            }
        });

    }



    @Override
    public int getItemCount() {
        return promoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_api_level;
        private TextView tv_name;
        private TextView img_link;
        private ImageView image;
        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tv_api_level = view.findViewById(R.id.tv_api_level);
            image = view.findViewById(R.id.img_promo);





        }
    }


}