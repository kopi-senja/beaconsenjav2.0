package com.example.retrofit_test;

public class MajorBeacon {
    private String major;
    private String minor;

    public String getMajor() {
        return major;
    }


    public String getMinor() {

        return minor;
    }


    public MajorBeacon(String major, String minor) {
        this.major = major;
        this.minor = minor;
    }
}
